//this HAS to be a factory class. THere are going to be too many methods that will be shared with other
// classes. EX: Reporting class, exceptions, and similar csv read/write methods.

package keyworddriventests;

import opencsv.*;

public class OtherTests {
	public OtherTests(String filename, CSVReader readerobj){
		System.out.println("IN OtherTests() class, received filename " + filename);
	}
	public void runTest(){
		
	}
}
