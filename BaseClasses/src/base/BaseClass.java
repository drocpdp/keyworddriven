//test comment
package base;
import org.openqa.selenium.*;
import org.openqa.selenium.ie.*;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.htmlunit.*;
import org.openqa.selenium.chrome.*;


public abstract class BaseClass {
	
	//instance variables
	public WebDriver webdriver; 
	//------------------
	
	/**
	 * Constructor
	 */
	public BaseClass(){
		this.instantiateWebDriverObject(1);
	}

	/**
	 * Constructor
	 * @param i an indexed integer that directs what browser to run test on. 1 = Firefox, 2 = IE, 3 = HtmlUnit Driver, 4 = Chrome Driver, default = FF
	 */
	public BaseClass(int i){
		this.instantiateWebDriverObject(i);
	}
	
	
	private void instantiateWebDriverObject(int i) {
		System.out.println("Entering instantiateWebDriverObject()");
		System.out.println("Received value of " + Integer.toString(i));
		switch(i){
			case 1: 
			{
				System.out.println("--Firefox");
				webdriver = new FirefoxDriver();
				break;
			}
			case 2:
			{
				System.out.println("--Internet Explorer");
				webdriver = new InternetExplorerDriver();
				break;
			}
			case 3:
			{
				System.out.println("--HtmlUnit Driver");
				webdriver = new HtmlUnitDriver();
				break;
			}
			case 4:
			{
				System.out.println("--Chrome Driver");
				webdriver = new ChromeDriver();
				break;
			}
			default:
			{
				System.out.println("--DEFAULTING TO Firefox");
				webdriver = new FirefoxDriver();
				break;
			}
		}
	}
	
}
