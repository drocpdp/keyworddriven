package base;
// Open file (hopefully, csv file) <-- this is taken care of in the pass-through
// On sheet, check what test to run (first row, first cell)
// If attribute checker, pass [???filename/file???] to separate class

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import opencsv.*;
import keyworddriventests.AttributeTests;
import keyworddriventests.OtherTests;

public class KeywordDriven {
	
	//instance variables
	String fqfilename;
	Logger log;
	
	//instantiation method.
	public KeywordDriven(String filename){
		
		boolean isNewLog = true;
		log = new Logger(isNewLog);
		
		//add current test name to log file
		log.saveTestName(filename);
		
		System.out.println("RECEIVED FILENAME:" + filename);
		
		//lets assign it to the instance variable so we can use it in other methods
		fqfilename = filename;
		
		//placeholder for iteration through each row of .csv file
		String[] nextline;
		
	    try {
	    	//lets create new CSVReader object using supplied file name.
	    	//keep in the try-catch structure since this is prone to exception.
			CSVReader readerobject = new CSVReader(new FileReader(filename));
			
			//get first line (only) of csv file. This should contain name of type of test we should run
			nextline = readerobject.readNext();
			String testToRun = nextline[0];
			System.out.println(testToRun);		
			
			//get 2nd line. This is description.
			nextline = readerobject.readNext();
			String testDescription = nextline[0];
			System.out.println(testDescription);
			log.saveTestDescription(testDescription);
			
			//Ok, now prepare file to be passed into appropriate test
			runTest(testToRun, readerobject);
		
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void runTest(String testType, CSVReader readerobject){
		System.out.println("Received  [" + testType + "]");

		if (testType.equals("attribute")){
			AttributeTests test = new AttributeTests(fqfilename, readerobject);
			log.saveTestType(testType);
			test.runTest();
		}
		if (testType.equals("other")){
			OtherTests test = new OtherTests(fqfilename, readerobject);
			log.saveTestType(testType);
			test.runTest();
		}
		
		
	}
	

}
