//this HAS to be a factory class. There are going to be too many methods that will be shared with other
// classes. EX: Reporting class, exceptions, and similar csv read/write methods.

//Steps. First write and test as normal class
//
//Then transfer over to factory class (KeywordDrivenTest.java) and 
//
//create new AttributeTests.java to inherit and override.

package keyworddriventests;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import base.TCase;

import opencsv.*;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class AttributeTests {
	
	String[] nextLine;
	CSVReader readerobject;
	TCase tc;
	WebElement we; //current web element object instance-wide

	//This should be abstracted
	public AttributeTests(String filename, CSVReader readerobj){
		System.out.println("In AttributeTests() class");		
		readerobject = readerobj;
		openBrowser();
	}
	
	//This should be abstracted
	public AttributeTests(String filename){
	    try {
			readerobject = new CSVReader(new FileReader(filename));
			openBrowser();
				
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	
	//This should be abstracted
	public void iterateCSVTestSteps(CSVReader readerobject){
		iterateCSVTestSteps(readerobject,2);
	}
	
	//This should be abstracted. firstLine is 1-based index.
	public void iterateCSVTestSteps(CSVReader readerobject, int firstLine){
		
		int counter = 1;
		//iterate
	    try {
			while ((nextLine = readerobject.readNext()) != null) {
				if (counter > firstLine){
					determineAction(readerobject,nextLine);
				}
				counter++;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		closeBrowser();
	}
	
	//This should be abstracted and then overridden
	private void determineAction(CSVReader readerobject, String[] currentLine) throws IOException{
		String currentAction = currentLine[0];
		System.out.println(currentAction);
		
		if (currentAction.equals("NAVIGATE")){
			navigateTo(currentLine[1]);
		}
		if (currentAction.equals("VERIFY")){
			System.out.println("RECEIVED A VERIFY");
			verifyAttribute(currentLine);
		}
	}
	
	private boolean verifyAttribute(String[] line) throws IOException{
		//innocent until proven guilty
		boolean isExists = true;
		//separate these two so we can return separate log entries
			if (!(isAttributeExists(line))){
				isExists = false;
			}
		return isExists;
	}

	private boolean isAttributeExists(String[] line) throws IOException{
		return isAttributeExists(line[2], line[3], line[4], line[5]);
	}
	
	private boolean isAttributeExists(String objName, String objValue, String attributename, String attributevalue) throws IOException{
		System.out.println("OBJIDNAME" + "=" + objName);
		System.out.println("OBJIDVALUE" + "=" + objValue);
		System.out.println("EXPECTEDATTRIBUTENAME" + "=" + attributename);
		System.out.println("EXPECTEDATTRIBUTEVALUE" + "=" + attributevalue);
		
		if (!(isElementExists(objName, objValue))){return false;}
		
		//isAttributeExists work here
		System.out.println("ATTRIBUTE-->");
		String currentAttribute = we.getAttribute(attributename);
		System.out.println(currentAttribute);
		System.out.println("DONE ATTRIBUTE");
		if (currentAttribute.equals(attributevalue)){
			System.out.println("MATCHES");
			System.out.println("EXPECTING:[" + attributevalue + "] -- ACTUAL:[" + currentAttribute + "]");
			tc.log.saveLogEntry("pass", attributevalue, currentAttribute, "This should match", getCurrentDateTime());
		}
		else{
			System.out.println("DOES NOT MATCH!");
			System.out.println("EXPECTING:[" + attributevalue + "] -- ACTUAL:[" + currentAttribute + "]");
			tc.log.saveLogEntry("fail", attributevalue, currentAttribute, "This should match", getCurrentDateTime());
		}
		
		return true;
	}	
	
	/*
	private boolean isElementExists(String[] line){
		return isElementExists(line[2], line[3]);
		
	}
	*/
	
	private boolean isElementExists(String name, String value){
		System.out.println("IDENTIFIERNAME" + "=" + name);
		System.out.println("IDENTIFIERVALUE" + "=" + value);	
		//pass element after found to isAttributeExists() ???
		
		System.out.println("Finding");
		we = tc.webdriver.findElement(By.name(value));
		System.out.println("Finding done");
		
		return true;
	}
	
		
	private void navigateTo(String url) throws IOException{
		System.out.println("Navigating to [" + url + "]");
		tc.webdriver.get(url);
		tc.log.saveLogEntry("", "", "", "navigating to " + url, getCurrentDateTime());
	}
	
	private void openBrowser(){
		tc = new TCase();
		System.out.println("Opening new browser");
	}

	private void closeBrowser(){
		tc.webdriver.close();
		System.out.println("Closing existing browser instance");
	}
	
	//Definitely abstracted and overridden.
	public void runTest(){
		iterateCSVTestSteps(readerobject);
	}
	
	public Date getCurrentDateTime(){
		Date date = new Date();
		return date;
	}
	
}
