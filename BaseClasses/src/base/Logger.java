package base;

import java.io.*;

import java.util.Date;

public class Logger {
	
	String currentLogDirectory = "/Users/deynon/Desktop";
	String filename = "tempresults";
	String logfile;
	String testname;
	Date testdatetime;
	Date currentlogentrystart;
	String testdescription;
	FileWriter fw;
	
	public Logger(boolean isThisNewLog){
		createLogObject(isThisNewLog);
	}
	
	public Logger(){
		createLogObject(false);
	}
	
	private void createLogObject(boolean isNewLog){
		logfile = currentLogDirectory + "/" + filename;
		try {
			if (isNewLog){
				fw = new FileWriter(logfile);
			}
			else{
				//this variable just for clarity
				boolean doAppend = true;
				fw = new FileWriter(logfile,doAppend);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	
	public void testLogger(){
		System.out.println("TEST OF LOGGER SUCCESSFUL");
		try {
			fw.append("Test of Logger successful" + (char)12);
			fw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void saveTestName(String name){
		testname = name;
		System.out.println("in saveTestName() - testname =" + testname);
		try {
			fw.append("in saveTestName() - testname =" + testname + (char)12);
			fw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	public void saveTestType(String type){
		String testtype = type;
		System.out.println("in saveTestType() and testtype =" + testtype);
		try {
			fw.append("in saveTestType() and testtype =" + testtype + (char)12);
			fw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}	
	public void saveTestStartDateTime(Date datetime) throws IOException{
		testdatetime = datetime;
		System.out.println("in saveTestStartDateTime() - testdatetime =" + testdatetime.toString());
		try {
			fw.append("in saveTestStartDateTime() - testdatetime =" + testdatetime.toString() + (char)12);
			fw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void saveTestDescription(String description) throws IOException{
		testdescription = description;
		System.out.println("in saveTestDescription() - testdescription =" + testdescription);
		try {
			fw.append("in saveTestDescription() - testdescription =" + testdescription + (char)12);
			fw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void saveDateTimeNow(Date now) throws IOException{
		currentlogentrystart = now;
		System.out.println("in saveDateTimeNow() - currentlogentrystart =" + now.toString());
		try {
			fw.append("in saveDateTimeNow() - currentlogentrystart =" + now.toString() + (char)12);
			fw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void saveLogEntry(String passfailwarn, String expected, String actual, String description, Date datetime) throws IOException{
		System.out.println("Received:" + passfailwarn + " " + expected + " " + actual + " " + description + " " + datetime.toString());
		try {
			fw.append(passfailwarn + " " + expected + " " + actual + " " + description + " " + datetime.toString() + (char)12);
			fw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
