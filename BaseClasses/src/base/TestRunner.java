package base;
import java.io.File;


public class TestRunner{
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) {
				
		//where directory is. This should somehow be later made to be read from config file.
		String username = System.getProperty("user.home");
		String os = System.getProperty("os.name");
		System.out.println(os);
		System.out.println(username);
		String testDirectory = username + "/Desktop/tests";
		
		//Open File object 
		File dir = new File(testDirectory);
		
		//get all files in directory
		String[] fileList = dir.list();
		
		//iterate through all filenames
		for (int a = 0; a < fileList.length; a++){
			
			//getting current file name
			String fileName = fileList[a];
			
			//check for correct file extension (which denotes correct file type)
			String fileExtension = getExtension(fileName);
			
			//create fully qualified file name string
			String fqname = testDirectory + "/" + fileName;
			
			//should be csv file
			if (fileExtension.equals("csv")){			
				//pass fully qualified file name to KeywordDriven class and instantiate new object.
				KeywordDriven keyword = new KeywordDriven(fqname);
			}
			else{
				System.out.println("WARNING: Unwanted file (incorrect file extension)");
				System.out.println("WARNING: Skipping file:[" + fqname + "]");
			}
		}		
	}
	
	
	//private helper class which extracts file extension
	private static String getExtension(String fullfilename){
		int dot = fullfilename.lastIndexOf(".");
		return fullfilename.substring(dot + 1);
	}

}